const express = require("express");
const uuid = require("uuid");
const router = express.Router();
const notes = require("../../Notes");

// Gets all notes
router.get("/", (req, res) => {
  res.json(notes);
});

// Get Single Note
router.get("/:id", (req, res) => {
  const found = notes.some((note) => note.id === parseInt(req.params.id));

  if (found) {
    res.json(notes.filter((note) => note.id === parseInt(req.params.id)));
  } else {
    res.status(400).json({ msg: `No note with the id of ${req.params.id}` });
  }
});

//Create Note
router.post("/", (req, res) => {
  const newNote = {
    id: uuid.v4(),
    name: req.body.name,
    note: req.body.note,
  };
  //res.send(req.body);

  if (!newNote.name || !newNote.note) {
    return res.status(400).json({ msg: "Please include a name and note" });
  }

  notes.push(newNote);
  res.json(notes);
  // res.redirect("/");
});

// Update Note
router.put("/:id", (req, res) => {
  const found = notes.some((note) => note.id === parseInt(req.params.id));

  if (found) {
    const updNote = req.body;
    notes.forEach((note) => {
      if (note.id === parseInt(req.params.id)) {
        note.name = updNote.name ? updNote.name : note.name;
        note.note = updNote.note ? updNote.note : note.note;

        res.json({ msg: "Note updated", note });
      }
    });
  } else {
    res.status(400).json({ msg: `No note with the id of ${req.params.id}` });
  }
});

// Delete Note
router.delete("/:id", (req, res) => {
  const found = notes.some((note) => note.id === parseInt(req.params.id));

  if (found) {
    res.json({
      msg: "Note deleted",
      notes: notes.filter((note) => note.id !== parseInt(req.params.id)),
    });
  } else {
    res.status(400).json({ msg: `No note with the id of ${req.params.id}` });
  }
});

module.exports = router;

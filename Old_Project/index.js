const express = require("express");
const path = require("path");
const exphbs = require("express-handlebars");
const logger = require("./middleware/logger");
const notes = require("./Notes");

const app = express();

//Init middleware
//app.use(logger);

// Handlebars Middleware
app.engine("handlebars", exphbs({ defaultLayout: "main" }));
app.set("view engine", "handlebars");

// Body Parser Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Homepage Route
app.get("/", (req, res) =>
  res.render("index", {
    title: "Note App",
    notes,
  })
);

//Set static folder
app.use(express.static(path.join(__dirname, "public")));

// Notes API Routes
app.use("/api/notes", require("./routes/api/notes"));

const PORT = process.env.PORT || 2000;

app.listen(PORT, () => console.log("Server started on port ", PORT));

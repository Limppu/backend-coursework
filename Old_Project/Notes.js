const notes = [
  {
    id: 1,
    name: "Grocery shopping list",
    note: "bananas, milk and ice cream for milkshakes",
  },
  {
    id: 2,
    name: "Birthday present list",
    note:
      "Adventure book for Tommy, football for Amy and toy soldier for Grandma",
  },
  {
    id: 3,
    name: "Work to-do-list",
    note: "Call bank, sign agreements and approve worker holidays",
  },
];

module.exports = notes;

-SDS Backend Coursework

-

-The tutorials and project for the SDS Backend course

-

-Getting Started

-

-These instructions will get you a copy of the project up and running on your local machine for testing purposes.

-

-Prerequisites

-

-What things you need to install the software and how to install them

-

-Visual Studio Code or similar tool to run the local server

-Postman or similar tool to manage the database

-

-Installing

-

-Install the mentioned programs from their respected sites to get the latest versions.

-

-https://code.visualstudio.com/

-https://www.postman.com/

-

-Running the tests

-

-Each project should be run from their seperate folder by navigating in the terminal with cd "folder" command. Only exception is the members ExpressJS module which is in the root folder.

-

-Course project is in the project folder.

-

-The database for the project is engaged with the command "npm run dev".

-

-With the database running the project can be opened in a browser by entering localhost:2000 to view the project.

-

-In the site the project presents preassigned notes, along with the form to add new notes. Once a note is filled the "Add Note" button can be used to submit the note to the database.

-

-After adding a note the project presents the API view where the new note can be viewed as been added to the database. 

-

-By returning to localhost:2000 the new note is also displayed as added and more notes can also be added.

-

-Updating or deleting notes

-

-Updating and deleting notes can be done by using Postman to access the database. 

-

-Updating notes

-

-In Postman by creating a new request and connecting to http://localhost:2000/api/notes/"chosen note id" the note can be edited.

-

-In the Headers tab a new header with the Key of "Content-Type" and the value of "application/json" is to be added.

-

-The update is accomplished by choosing the PUT method and adding to the wanted changes to the body/raw tab

-

-Example update in the body/raw tab:

-{

-    "name": "Super duper shopping list"

-}

-

-This changes the name of the chosen note. In this example that of http://localhost:2000/api/notes/1.

-

-Now the update can be applied with the Send button

-

-The update can be viewed in Postman or in browser at http://localhost:2000

-

-Deleting notes

-

-Deleting works similar to updating.

-

-In Postman by creating a new request and connecting to http://localhost:2000/api/notes/"chosen note id" the note to be deleted.

-

-In the Headers tab a new header with the Key of "Content-Type" and the value of "application/json" is to be added.

-

-Deleting is accomplished by choosing the DELETE method

-

-This changes the name of the chosen note. In this example that of http://localhost:2000/api/notes/1.

-

-Now the deletion can be applied with the Send button

-

-The update can be viewed in Postman or in browser at http://localhost:2000

-

-Built With

-

-    Visual Studio Code - The code editor used

-    Portman - Database management

-

-Author

-

-    Tuomas Liimatta